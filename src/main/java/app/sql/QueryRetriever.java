package app.sql;

import org.apache.commons.io.FileUtils;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;

@Configuration
public class QueryRetriever {

    public static String getQuery(String filename) {
        String path = QueryRetriever.class.getClassLoader().getResource(filename).getFile();
        try{
            File file = new File(path);
            return FileUtils.readFileToString(file, "UTF-8");
        }catch(IOException e){
            return null;
        }
    }

    public static String getUserByUsername() {
        return getQuery("sql/users/loadUsersByUsername.sql");
    }

    public static String getAuthoritiesByUsername() {
        return getQuery("sql/users/loadAuthoritiesByUsername.sql");
    }

    public static String getPersonById() {
        return getQuery("sql/people/getPersonById.sql");
    }

    public static String getAirline() {
        return getQuery("sql/airlines/getAirline.sql");
    }

    public static String getDiscountsByFlight() {
       return getQuery("sql/airlines/getDiscountsByAirline.sql");
    }

    public static String getAllCities() {
        return getQuery("sql/flights/getAllCities.sql");
    }

    public static String getFlight() {
        return getQuery("sql/flights/getFlightByAirlineAndNumber.sql");
    }

    public static String getAllFlights() {
        return getQuery("sql/flights/getAllFlights.sql");
    }

    public static String getFaresByFlight() {
        return getQuery("sql/fares/getFaresByFlight.sql");
    }

    public static String getStopsByTrip() {
        return getQuery("sql/stops/getStopsByTrip.sql");
    }

    public static String getLegsByFlight() {
        return getQuery("sql/legs/getLegsByFlight.sql");
    }

    public static String getTripDetails() {
        return getQuery("sql/trips/getOneWayTrips.sql");
    }

    public static String getRoundTripDetails() {
        return getQuery("sql/trips/getRoundTrips.sql");
    }

    public static String getTripById() {
        return getQuery("sql/trips/getTripById.sql");
    }

    public static String getAllCustomers() {
        return getQuery("sql/customers/getAllCustomers.sql");
    }

    public static String getCustomerByPerson() {
        return getQuery("sql/customers/getCustomerByPerson.sql");
    }

    public static String getCustomerByAccount() {
        return getQuery("sql/customers/getCustomerByAccountNo.sql");
    }

    public static String deleteCustomerByAccount() {
        return getQuery("sql/customers/deleteByAccountNo.sql");
    }

    public static String updateCustomer() {
        return getQuery("sql/customers/updateCustomer.sql");
    }

    public static String getEmployeeByPerson() {
        return getQuery("sql/employees/getEmployeeByPerson.sql");
    }

    public static String updateEmployee() {
        return getQuery("sql/employees/updateEmployee.sql");
    }

    public static String getAllEmployees(){
        return getQuery("sql/employees/getAllEmployees.sql");
    }

    public static String getEmployeeBySSN() {
        return getQuery("sql/employees/getEmployeeBySSN.sql");
    }

    public static String getReservationsByCustomer() {
        return getQuery("sql/reservations/getAllReservationsViaCustomer.sql");
    }

    public static String getReservationByResrNo() {
        return getQuery("sql/reservations/getReservationByResrNo.sql");
    }

    public static String insertCustomer() {
        return getQuery("sql/customers/insertCustomer.sql");
    }

    public static String insertRep() {
        return getQuery("sql/rep/insertRep.sql");
    }

    public static String insertManager() {
        return getQuery("sql/managers/insertManager.sql");
    }

    public static String insertNewReservation() {
        return getQuery("sql/reservations/insertNewReservation.sql");
    }

    public static String getMostActiveFlights() {
        return getQuery("sql/transactions/getListOfMostActiveFlights.sql");
    }

    public static String getListOfReserByTrip() {
        return getQuery("sql/transactions/getListOfReserByTrip.sql");
    }

    public static String getRepWithMostRevenue() {
        return getQuery("sql/transactions/getRepWithMostRevenue.sql");
    }

    public static String getResrByCustomerName() {
        return getQuery("sql/transactions/getResrByCustomerName.sql");
    }

    public static String getCusWithMostRev() {
        return getQuery("sql/transactions/getCusWithMostRevenue.sql");
    }
}
