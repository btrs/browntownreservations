package app.dao;

import app.entities.Person;
import app.sql.QueryRetriever;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

@Repository
public class PersonDAO {

    @Autowired
    private JdbcTemplate template;

    public Person getPerson(int id) {
        return template.queryForObject(QueryRetriever.getPersonById(), new PersonRowMapper(), id);
    }

    private class PersonRowMapper implements RowMapper<Person> {

        public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
            int id = rs.getInt("Id");
            String firstName = rs.getString("FirstName");
            String lastName = rs.getString("LastName");
            String address = rs.getString("Address");
            String city = rs.getString("City");
            String state = rs.getString("State");
            String zipCode = rs.getString("ZipCode");
            Person person = new Person();
            person.setId(id);
            person.setFirstName(firstName);
            person.setLastName(lastName);
            person.setAddress(address);
            person.setCity(city);
            person.setState(state);
            person.setZipCode(zipCode);
            return person;
        }
    }
}
