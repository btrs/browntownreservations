package app.dao;

import app.entities.Employee;
import app.entities.Person;
import app.sql.QueryRetriever;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeDAO {

    @Autowired
    private JdbcTemplate template;

    @Autowired
    private PersonDAO personDAO;

    public Employee getEmployee(Person person) {
        Employee employee = template.queryForObject(QueryRetriever.getEmployeeByPerson(), new PartialEmployeeRowMapper(), person.getId());
        employee.setPerson(person);
        return employee;
    }

    public List<Employee> getAllEmployees() {
        return template.queryForObject(QueryRetriever.getAllEmployees(), new RowMapper<List<Employee>>() {
            public List<Employee> mapRow(ResultSet rs, int rowNum) throws SQLException {
                List<Employee> employees = new ArrayList<Employee>();
                do {
                    employees.add(getEmployee(rs));
                }while(rs.next());
                return employees;
            }
        });
    }

    public Employee getEmployee(String ssn) {
        return template.queryForObject(QueryRetriever.getEmployeeBySSN(), new EmployeeRowMapper(), ssn);
    }

    public void update(Employee employee) {
        template.update(QueryRetriever.updateEmployee(),
                employee.getFirstName(),
                employee.getLastName(),
                employee.getAddress(),
                employee.getCity(),
                employee.getState(),
                employee.getZipCode(),
                employee.getSsn(),
                employee.getHourlyRate());
    }

    public void saveManager(Object ...args) {
        template.update(QueryRetriever.insertManager(), args);
    }

    public void saveRep(Object ...args) {
        template.update(QueryRetriever.insertRep(), args);
    }

    private Employee getEmployee(ResultSet rs) throws SQLException {
        Employee employee = setEmployeeFields(rs);
        Person person = personDAO.getPerson(rs.getInt("Id"));
        employee.setPerson(person);
        return employee;
    }

    private Employee setEmployeeFields(ResultSet rs) throws SQLException {
        String ssn = rs.getString("SSN");
        Double hourlyRate = rs.getDouble("HourlyRate");
        Employee employee = new Employee();
        employee.setSsn(ssn);
        employee.setHourlyRate(hourlyRate);
        return employee;
    }

    private class EmployeeRowMapper implements RowMapper<Employee> {

        public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
            return getEmployee(rs);
        }
    }

    private class PartialEmployeeRowMapper implements RowMapper<Employee> {

        public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
            return setEmployeeFields(rs);
        }
    }

}
