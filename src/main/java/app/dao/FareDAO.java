package app.dao;

import app.entities.Flight;
import app.entities.fares.Fare;
import app.sql.QueryRetriever;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Repository
public class FareDAO {

    @Autowired
    public JdbcTemplate template;

    public Fare getFareByFlight(String airlineId, int flightNo) {
        return template.queryForObject(QueryRetriever.getFaresByFlight(), new FareMapper(), airlineId, flightNo);
    }

    private class FareMapper implements RowMapper<Fare> {

        @Override
        public Fare mapRow(ResultSet rs, int rowNum) throws SQLException {
            Fare fare = new Fare();
            do{
                String fareType = rs.getString("FareType");
                String fareClass = rs.getString("Class");
                Double farePrice = rs.getDouble("Fare");
                fare.addFare(fareType, fareClass, farePrice);
            }while(rs.next());
            return fare;
        }
    }

}
