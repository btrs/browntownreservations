package app.dao;

import app.entities.Customer;
import app.entities.Person;
import app.sql.QueryRetriever;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class CustomerDAO {

    @Autowired
    private JdbcTemplate template;

    @Autowired
    private PersonDAO personDAO;

    public Customer getCustomer(Person person) {
        Customer customer = template.queryForObject(QueryRetriever.getCustomerByPerson(), new ParitalCustomerRowMapper(), person.getId());
        customer.setPerson(person);
        return customer;
    }

    public Customer getCustomer(int accountNo) {
        return template.queryForObject(QueryRetriever.getCustomerByAccount(), new CustomerRowMapper(), accountNo);
    }

    public List<Customer> getAllCustomers() {
        return template.query(QueryRetriever.getAllCustomers(), new CustomerRowMapper());
    }

    public void updateCustomer(Customer customer) {
        template.update(QueryRetriever.updateCustomer(),
                customer.getFirstName(),
                customer.getLastName(),
                customer.getAddress(),
                customer.getCity(),
                customer.getState(),
                customer.getZipCode(),
                customer.getAccountNo(),
                customer.getCreditCardNo(),
                customer.getEmail());
    }

    public void saveCustomer(Object ...args) {
        template.update(QueryRetriever.insertCustomer(), args);
    }

    public Customer setCustomerFields(ResultSet rs) throws SQLException {
        int accountNo = rs.getInt("AccountNo");
        String creditCardNo = rs.getString("CreditCardNo");
        String email = rs.getString("Email");
        Customer customer = new Customer(accountNo);
        customer.setCreditCardNo(creditCardNo);
        customer.setEmail(email);
        return customer;
    }

    /* returns the customer object with the person details */
    private class CustomerRowMapper implements RowMapper<Customer> {

        public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
            Customer customer = setCustomerFields(rs);
            Person person = personDAO.getPerson(rs.getInt("Id"));
            customer.setPerson(person);
            return customer;
        }
    }

    /* returns the customer object without the person details */
    private class ParitalCustomerRowMapper implements RowMapper<Customer> {

        public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
            return setCustomerFields(rs);
        }
    }
}
