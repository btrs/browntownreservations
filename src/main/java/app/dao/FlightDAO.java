package app.dao;

import app.entities.Airline;
import app.entities.Flight;
import app.entities.Leg;
import app.entities.fares.Fare;
import app.sql.QueryRetriever;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class FlightDAO {

    @Autowired
    private AirlineDAO airlineDAO;

    @Autowired
    private FareDAO fareDAO;

    @Autowired
    private JdbcTemplate template;

    public List<String> getAllCities() {
        return template.queryForObject(QueryRetriever.getAllCities(), new RowMapper<List<String>>() {
            public List<String> mapRow(ResultSet resultSet, int i) throws SQLException {
                List<String> cities = new ArrayList<String>();
                do{
                    cities.add(resultSet.getString("City"));
                }while(resultSet.next());
                return cities;
            }
        });
    }

    public Flight getFlight(String airlineId, int flightNo) {
        return template.queryForObject(QueryRetriever.getFlight(), new FlightRowMapper(), airlineId, flightNo);
    }

    public List<Flight> getAllFlights() {
        return template.queryForObject(QueryRetriever.getAllFlights(), new RowMapper<List<Flight>>() {
            public List<Flight> mapRow(ResultSet rs, int rowNum) throws SQLException {
                List<Flight> flights = new ArrayList<Flight>();
                do{
                   flights.add(getFlight(rs));
                }while(rs.next());
                return flights;
            }
        });
    }

    private Flight getFlight(ResultSet rs) throws SQLException {
        Airline airline = airlineDAO.getAirline(rs.getString("AirlineID"));
        int flightNo = rs.getInt("FlightNo");
        Flight flight = new Flight(airline, flightNo);
        flight.setNoOfSeats(rs.getInt("NoOfSeats"));
        Fare fare = fareDAO.getFareByFlight(airline.getId(), flightNo);
        flight.setFare(fare);
        return flight;
    }

    private class FlightRowMapper implements RowMapper<Flight> {

        public Flight mapRow(ResultSet rs, int rowNum) throws SQLException {
            return getFlight(rs);
        }
    }
}
