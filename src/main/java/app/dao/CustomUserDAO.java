package app.dao;

import app.entities.CustomUser;
import app.entities.Person;
import app.sql.QueryRetriever;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
public class CustomUserDAO implements UserDetailsService {

    @Autowired
    private JdbcTemplate template;

    @Autowired
    private PersonDAO personDAO;

    public CustomUser loadUserByUsername(String username) throws UsernameNotFoundException {
        CustomUser user = null;
        try{
            user = template.queryForObject(QueryRetriever.getUserByUsername(), new CustomUserRowMapper(), username);
        }catch(EmptyResultDataAccessException e) {
            throw new UsernameNotFoundException("No user with that username was found");
        }
        return user;
    }

    public Collection<GrantedAuthority> loadAuthoritiesByUsername(String username) {
        return template.queryForObject(QueryRetriever.getAuthoritiesByUsername(), new AuthorityMapper(), username);
    }

    private class AuthorityMapper implements RowMapper<Collection<GrantedAuthority>> {

        public Collection<GrantedAuthority> mapRow(ResultSet rs, int rowNum) throws SQLException {
            Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            do{
                GrantedAuthority auth = new SimpleGrantedAuthority(rs.getString("Authority"));
                authorities.add(auth);
            }while(rs.next());
            return authorities;
        }
    }

    private class CustomUserRowMapper implements RowMapper<CustomUser> {

        public CustomUser mapRow(ResultSet rs, int rowNum) throws SQLException {
            String username = rs.getString("Username");
            String password = rs.getString("Password");
            Collection<GrantedAuthority> authorities = loadAuthoritiesByUsername(username);
            int personID = rs.getInt("PersonID");
            CustomUser user = new CustomUser(username, password, authorities);
            Person person = personDAO.getPerson(personID);
            user.setPerson(person);
            return user;
        }
    }

}
