package app.dao;

import app.entities.Stop;
import app.sql.QueryRetriever;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class StopDAO {

    @Autowired
    private JdbcTemplate template;

    public List<Stop> getStops(int tripID) {
        List<Stop> stops = template.queryForObject(QueryRetriever.getStopsByTrip(), new RowMapper<List<Stop>>() {
            public List<Stop> mapRow(ResultSet rs, int rowNum) throws SQLException {
                List<Stop> stops = new ArrayList<Stop>();
                do {
                    stops.add(getStop(rs));
                }while(rs.next());
                return stops;
            }
        }, tripID);
        return stops;
    }

    public Stop getStop(ResultSet rs) throws SQLException {
        Stop stop = new Stop();
        stop.setStopNo(rs.getInt("StopNo"));
        stop.setArrTime(rs.getTimestamp("ArrTime"));
        stop.setDepTime(rs.getTimestamp("DepTime"));
        stop.setActualArrTime(rs.getTimestamp("ActualArrTime"));
        stop.setActualArrTime(rs.getTimestamp("ActualDepTime"));
        stop.setAirportID(rs.getString("AirportID"));
        return stop;
    }

    private class StopMapper implements RowMapper<Stop> {

        public Stop mapRow(ResultSet rs, int rowNum) throws SQLException {
            return getStop(rs);
        }
    }
}
