package app.dao;

import app.entities.Airline;
import app.entities.Discount;
import app.sql.QueryRetriever;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AirlineDAO {

    @Autowired
    JdbcTemplate template;

    public Airline getAirline(String id) {
        return template.queryForObject(QueryRetriever.getAirline(), new AirlineRowMapper(), id);
    }

    private List<Discount> getDiscountsByAirline(Airline airline) {
        return template.queryForObject(QueryRetriever.getDiscountsByFlight(), new RowMapper<List<Discount>>() {
            public List<Discount> mapRow(ResultSet rs, int rowNum) throws SQLException {
                List<Discount> discounts = new ArrayList<Discount>();
                do {
                    discounts.add(new Discount(rs.getInt("Days"), rs.getDouble("Discount")));
                }while(rs.next());
                return discounts;
            }
        }, airline.getId());
    }

    private Airline getAirline(ResultSet rs) throws SQLException {
        String id = rs.getString("Id");
        String name = rs.getString("Name");
        Airline airline = new Airline(id, name);
        List<Discount> discounts = getDiscountsByAirline(airline);
        airline.setDiscounts(discounts);
        return airline;
    }

    private class AirlineRowMapper implements RowMapper<Airline> {

        public Airline mapRow(ResultSet rs, int rowNum) throws SQLException {
            return getAirline(rs);
        }
    }


}
