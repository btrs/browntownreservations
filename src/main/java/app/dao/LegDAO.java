package app.dao;

import app.entities.Flight;
import app.entities.Leg;
import app.sql.QueryRetriever;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class LegDAO {

    @Autowired
    JdbcTemplate template;

    public List<Leg> getLegsByTrip(int resrNo) {
        return template.queryForObject(QueryRetriever.getLegsByFlight(), new RowMapper<List<Leg>>() {
            public List<Leg> mapRow(ResultSet rs, int rowNum) throws SQLException {
                List<Leg> legs = new ArrayList<Leg>();
                do{
                    legs.add(getLeg(rs));
                }while(rs.next());
                return legs;
            }
        }, resrNo);
    }

    private Leg getLeg(ResultSet rs) throws SQLException {
        Leg leg = new Leg();
        leg.setArrAirportID(rs.getString("ArrAirport"));
        leg.setDeptAirportID(rs.getString("DepAirport"));
        leg.setArrTime(rs.getTimestamp("DestArrTime"));
        leg.setDeptTime(rs.getTimestamp("DepTime"));
        leg.setTripID(rs.getInt("TripID"));
        return leg;
    }
}
