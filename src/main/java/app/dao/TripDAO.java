package app.dao;

import app.entities.Stop;
import app.entities.Trip;
import app.sql.QueryRetriever;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TripDAO {

    @Autowired
    private JdbcTemplate template;

    @Autowired
    private StopDAO stopDAO;

    public List<String> getOneWayTripDetails(String origin, String dest, String depDate, String arrDate) {
        return template.queryForObject(QueryRetriever.getTripDetails(), new TripDetailsRowMapper(), origin, dest, depDate, arrDate);
    }

    public List<String> getRoundTripDetails(String origin, String dest, String depDate, String arrDate) {
        return template.queryForObject(QueryRetriever.getRoundTripDetails(), new TripDetailsRowMapper(), origin, dest, depDate, arrDate);
    }

    public Trip getTrip(int tripID) {
        return template.queryForObject(QueryRetriever.getTripById(), new TripRowMapper(), tripID);
    }

    private class TripRowMapper implements RowMapper<Trip> {

        public Trip mapRow(ResultSet rs, int rowNum) throws SQLException {
            String airlineID = rs.getString("AirlineID");
            int flightNo = rs.getInt("FlightNo");
            int tripID = rs.getInt("TripID");
            List<Stop> stops = stopDAO.getStops(tripID);
            Trip trip = new Trip();
            trip.setAirlineID(airlineID);
            trip.setFlightNo(flightNo);
            trip.setStops(stops);
            trip.setAirlineID(airlineID);
            trip.setId(tripID);
            return trip;
        }
    }

    private class TripDetailsRowMapper implements RowMapper<List<String>> {

        public List<String> mapRow(ResultSet rs, int rowNum) throws SQLException {
            List<String> trips = new ArrayList<String>();
            do{
                trips.add(rs.getString("TripID"));
            }while(rs.next());
            return trips;
        }
    }
}
