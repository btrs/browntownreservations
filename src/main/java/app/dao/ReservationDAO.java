package app.dao;

import app.entities.Customer;
import app.entities.Leg;
import app.entities.Reservation;
import app.entities.ReservationDetails;
import app.sql.QueryRetriever;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ReservationDAO {

    @Autowired
    private JdbcTemplate template;

    @Autowired
    private EmployeeDAO employeeDAO;

    @Autowired
    private CustomerDAO customerDAO;

    @Autowired
    private LegDAO legDAO;

    public Reservation getReservation(int resrNo) {
        return template.queryForObject(QueryRetriever.getReservationByResrNo(), new ReservationMapper(),resrNo);
    }

    public void insertNewReservation(int accountNum, int tripID, int startStop, int endStop, double farePrice, double bookingFee, String repSsn) {
        template.update(QueryRetriever.insertNewReservation(), accountNum, tripID, startStop, endStop, farePrice, bookingFee, repSsn);
    }

    public List<ReservationDetails> getAllReservationsForCustomer(int accountNo) {
        return template.query(QueryRetriever.getReservationsByCustomer(), new ReservationDetailsMapper(), accountNo);
    }

    private class ReservationMapper implements RowMapper<Reservation> {

        @Override
        public Reservation mapRow(ResultSet rs, int rowNum) throws SQLException {
            Reservation reservation = new Reservation();
            reservation.setResrNo(rs.getInt("ResrNo"));
            reservation.setResrDate(rs.getTimestamp("ResrDate"));
            reservation.setBookingFee(rs.getDouble("BookingFee"));
            reservation.setTotalFare(rs.getDouble("TotalFare"));
            String repSSN = rs.getString("RepSSN");
            int accountNo = rs.getInt("AccountNo");
            if(repSSN != null) {
                reservation.setEmployee(employeeDAO.getEmployee(repSSN));
            }
            Customer c = customerDAO.getCustomer(accountNo);
            reservation.setCustomer(c);
            List<Leg> legs = legDAO.getLegsByTrip(reservation.getResrNo());
            reservation.setLegs(legs);
            return reservation;
        }
    }

    private class ReservationDetailsMapper implements RowMapper<ReservationDetails> {

        @Override
        public ReservationDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
            ReservationDetails details = new ReservationDetails();
            details.setAccountNo(rs.getInt("AccountNo"));
            details.setResrNo(rs.getInt("ResrNo"));
            return details;
        }
    }
}
