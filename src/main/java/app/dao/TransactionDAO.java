package app.dao;

import app.sql.QueryRetriever;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TransactionDAO {

    @Autowired
    private JdbcTemplate template;

    public List<Map<String, Object>> getMostActiveFlights() {
        return template.query(QueryRetriever.getMostActiveFlights(), new GenericResultMapper());
    }

    public List<Map<String, Object>> getListOfReserByTrip(int tripID) {
        return template.query(QueryRetriever.getListOfReserByTrip(), new GenericResultMapper(), tripID);
    }

    public Map<String, Object> getRepWithMostRevenue() {
        return template.queryForObject(QueryRetriever.getRepWithMostRevenue(), new GenericResultMapper());
    }

    public List<Map<String, Object>> getResrByCustomerName(String firstName, String lastName) {
        return template.query(QueryRetriever.getResrByCustomerName(), new GenericResultMapper(), firstName, lastName);
    }

    public Map<String, Object> getCusWithMostRevenue() {
        return template.queryForObject(QueryRetriever.getCusWithMostRev(), new GenericResultMapper());
    }

    private class GenericResultMapper implements RowMapper<Map<String, Object>> {

        @Override
        public Map<String, Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
            Map<String, Object> map = new HashMap<>();
            ResultSetMetaData rsmd = rs.getMetaData();
            int length = rsmd.getColumnCount();
            for(int i = 1; i <= length; i++) {
                String name = rsmd.getColumnName(i);
                Object data = rs.getObject(name);
                map.put(name, data);
            }
            return map;
        }
    }
}
