package app.controllers;

import app.dao.TripDAO;
import app.entities.TripDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/flightTrip")
public class TripServiceController {

    @Autowired
    TripDAO tripDAO;

    @RequestMapping("/oneway")
    public Object getOneWayDetails(@RequestParam("start") String origin,
                                              @RequestParam("stop") String destination,
                                              @RequestParam("dep") String depDate,
                                              @RequestParam("arr") String arrDate) {
        try {
            return tripDAO.getOneWayTripDetails(origin, destination, depDate, arrDate);
        }catch(EmptyResultDataAccessException e) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("error", "no flights match this criteria");
            return map;
        }
    }

    @RequestMapping("/round")
    public Object getRoundTripDetails(@RequestParam("start") String origin,
                                              @RequestParam("stop") String destination,
                                              @RequestParam("dep") String depDate,
                                              @RequestParam("arr") String arrDate){
        try {
            return tripDAO.getRoundTripDetails(origin, destination, depDate, arrDate);
        }catch(EmptyResultDataAccessException e) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("error", "no flights match this criteria");
            return map;
        }
    }
}
