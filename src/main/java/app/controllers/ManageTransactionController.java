package app.controllers;

import app.dao.TransactionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/transact")
public class ManageTransactionController {

    @Autowired
    TransactionDAO transactionDAO;

    @RequestMapping("/getListOfMostActiveFlights")
    public Object activeFlights() {
        try {
            return transactionDAO.getMostActiveFlights();
        }catch(EmptyResultDataAccessException e) {
            Map<String, Object> result = new HashMap<>();
            result.put("error", "Active flights could not be queried");
            return result;
        }
    }

    @RequestMapping("/getListOfReserByTrip")
    public Object getReserByTrip(@RequestParam("tripID") int tripID) {
        try {
            return transactionDAO.getListOfReserByTrip(tripID);
        }catch(EmptyResultDataAccessException e) {
            Map<String, Object> result = new HashMap<>();
            result.put("error", "Could not get reservation list");
            return result;
        }
    }

    @RequestMapping("/getRepWithMostRev")
    public Object getRepWithMostRev() {
        try {
            return transactionDAO.getRepWithMostRevenue();
        }catch(EmptyResultDataAccessException e) {
            Map<String, Object> result = new HashMap<>();
            result.put("error", "Could not find customer representative that generated the most revenue");
            return result;
        }
    }

    @RequestMapping("/getReserByCustomerName")
    public Object getReserByCustomerName(@RequestParam("firstName") String firstName,
                                         @RequestParam("lastName") String lastName) {
        try {
            return transactionDAO.getResrByCustomerName(firstName, lastName);
        }catch(EmptyResultDataAccessException e) {
            Map<String, Object> result = new HashMap<>();
            result.put("error", "Could not get reservation");
            return result;
        }
    }

    @RequestMapping("/getCusWithMostRevenue")
    public Object getCusWithMostRevenue() {
        try {
            return transactionDAO.getCusWithMostRevenue();
        }catch(EmptyResultDataAccessException e) {
            Map<String, Object> result = new HashMap<>();
            result.put("error", "Could not get reservation");
            return result;
        }
    }
}
