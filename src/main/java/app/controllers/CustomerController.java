package app.controllers;

import app.dao.*;
import app.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerDAO customerDAO;

    @Autowired
    private ReservationDAO reservationDAO;

    @Autowired
    private EmployeeDAO employeeDAO;

    @Autowired
    private FlightDAO flightDAO;

    @Autowired
    private TripDAO tripDAO;

    @RequestMapping("/all")
    public String getAllCustomers(Model model, RedirectAttributes redAtt) {
        try {
            List<Customer> customers = customerDAO.getAllCustomers();
            model.addAttribute("customers", customers);
            return "customer/all";
        }catch(EmptyResultDataAccessException e) {
            redAtt.addFlashAttribute("error", "No customers have signed up");
            return "redirect:/showError";
        }
    }

    @RequestMapping("/self")
    public String getSelf() {
        CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Person person = user.getPerson();
        Customer c = customerDAO.getCustomer(person);
        return "redirect:/customer/view/" + c.getAccountNo();
    }

    @RequestMapping("/self/reservations")
    public String getSelfRes() {
        CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Person person = user.getPerson();
        Customer c = customerDAO.getCustomer(person);
        return "redirect:/customer/view/" + c.getAccountNo() + "/reservations";
    }

    @RequestMapping("/register")
    public String register(@RequestParam(value="error", required=false) String error, Model model) {
        if(error != null)
            model.addAttribute("error", true);
        return "customer/register";
    }

    @RequestMapping(value="/register", method= RequestMethod.POST)
    public String register(@RequestParam("username") String username,
                         @RequestParam("password") String password,
                         @RequestParam("firstname") String firstName,
                         @RequestParam("lastname") String lastName,
                         @RequestParam("address") String address,
                         @RequestParam("city") String city,
                         @RequestParam("state") String state,
                         @RequestParam("zipcode") String zipCode,
                         @RequestParam("creditcardno") String creditCardNo,
                         @RequestParam("email") String email) {
        // TODO - do validation
        try{
            customerDAO.saveCustomer(firstName, lastName, address, city, state, zipCode, username, password, creditCardNo, email);
        }catch(Exception e) {
            e.printStackTrace();
            return "redirect:/customer/register?error=TRUE";
        }
        return "redirect:/login";
    }

    @RequestMapping("/view/{accountNo}")
    public String view(@PathVariable("accountNo") int accountNo, Model model) {
        CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Person person = user.getPerson();
        Customer customer = null;
        try {
            customer = customerDAO.getCustomer(accountNo);
        }catch(Exception e) {
            e.printStackTrace();
            model.addAttribute("error", "No such customer exists");
            return "redirect:/showError";
        }
        if(customer.getId() == person.getId() || !user.hasAuthority("CUSTOMER")) {
            model.addAttribute("customer", customer);
            return "customer/view";
        }else {
            model.addAttribute("error", "Cannot access this page");
            return "redirect:/showError";
        }
    }

    @RequestMapping(value="/view/{accountNo}/update", method=RequestMethod.POST)
    public String postView(@PathVariable("accountNo") int accountNo,
                           @RequestParam("firstname") String firstname,
                           @RequestParam("lastname") String lastName,
                           @RequestParam("address") String address,
                           @RequestParam("city") String city,
                           @RequestParam("state") String state,
                           @RequestParam("zipcode") String zipCode,
                           @RequestParam("creditcardno") String creditCardNo,
                           @RequestParam("email") String email,
                           Model model) {
        CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Person person = user.getPerson();
        Customer customer = customerDAO.getCustomer(accountNo);
        setCustomerDetails(customer, firstname, lastName, address,city, state, zipCode, creditCardNo, email);
        try {
            customerDAO.updateCustomer(customer);
        }catch(Exception e) {
            e.printStackTrace();
            model.addAttribute("error", "Could not update the customer");
            return "redirect:/showError";
        }
        return "redirect:/customer/view/" + accountNo;
    }

    @RequestMapping("/view/{accountNo}/reservations")
    public String getCustomerReservations(@PathVariable("accountNo") int accountNo,
                                          RedirectAttributes redAtt,
                                          Model model) {
        try{
            List<ReservationDetails> reservations = reservationDAO.getAllReservationsForCustomer(accountNo);
            model.addAttribute("reservations", reservations);
            return "reservations/allForCustomer";
        }catch(EmptyResultDataAccessException e) {
            redAtt.addFlashAttribute("error", "No reservations have been made yet");
            return "redirect:/showError";
        }
    }

    @RequestMapping("/view/{accountNo}/update")
    public String update(@PathVariable("accountNo") int accountNo, Model model) {
        CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Person person = user.getPerson();
        Customer customer = null;
        try {
            customer = customerDAO.getCustomer(accountNo);
        }catch(Exception e) {
            e.printStackTrace();
            return "redirect:/showError";
        }
        if(person.getId() == customer.getId() || !user.hasAuthority("CUSTOMER")) {
            model.addAttribute("customer", customer);
            return "customer/update";
        }else {
            model.addAttribute("error", "Not authorized");
            return "redirect:/showError";
        }
    }

    @RequestMapping(value="/reservations/new", method=RequestMethod.POST)
    public String postReservation(@RequestParam("accountNo") int accountNo,
                                  @RequestParam("tripID") int tripID,
                                  @RequestParam("startStop") int startStop,
                                  @RequestParam("endStop") int endStop,
                                  @RequestParam("airlineID") String airlineID,
                                  @RequestParam("flightNo") int flightNo,
                                  @RequestParam("seatClass") String seatClass,
                                  RedirectAttributes redAtt,
                                  Model model) {
        CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Person person = user.getPerson();
        Customer reservee = null;
        String repSSN = null;
        if(startStop >= endStop) {
            redAtt.addFlashAttribute("error", "start stop must be less than end stop");
            return "redirect:/showError";
        }
        try{
            if(accountNo == -1) {
                reservee = customerDAO.getCustomer(person);
            }else {
                reservee = customerDAO.getCustomer(accountNo);
                repSSN = employeeDAO.getEmployee(person).getSsn();
            }
        }catch(EmptyResultDataAccessException e) {
            redAtt.addFlashAttribute("error", "no such customer");
            return "redirect:/showError";
        }
        Flight f = flightDAO.getFlight(airlineID, flightNo);
        Trip t = tripDAO.getTrip(tripID);
        String fareType = (t.getStops().get(startStop-1).getAirportID().equals(t.getStops().get(endStop-1).getAirportID())) ? "round" : "oneway";
        double price = f.getFare().getPrice(fareType, seatClass);
        LocalDateTime today = LocalDateTime.now();
        LocalDateTime depDate = t.getStops().get(startStop).getDepTime().toLocalDateTime();
        int days = Math.abs((int)Duration.between(depDate, today).toDays());
        price = Math.abs(price - (price * f.getAirline().getDiscount(days)));
        try {
            reservationDAO.insertNewReservation(reservee.getAccountNo(), tripID, startStop, endStop, price, 10.00, repSSN);
        }catch(Exception e) {
            redAtt.addFlashAttribute("error", "You already have a reservation for this trip");
            return "redirect:/showError";
        }
        // TODO - redirect to reservations when you get that
        return "redirect:/";
    }

    private void setCustomerDetails(Customer customer, String ...args) {
        customer.setFirstName(args[0]);
        customer.setLastName(args[1]);
        customer.setAddress(args[2]);
        customer.setCity(args[3]);
        customer.setState(args[4]);
        customer.setZipCode(args[5]);
        customer.setCreditCardNo(args[6]);
        customer.setEmail(args[7]);
    }
}
