package app.controllers;

import app.dao.EmployeeDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/rep")
public class RepController {

    @Autowired
    private EmployeeDAO employeeDAO;

    @RequestMapping("/register")
    public String register(@RequestParam(value="error", required=false) String error, Model model) {
        if(error != null)
            model.addAttribute("error", true);
        return "rep/register";
    }

    @RequestMapping(value="/register", method= RequestMethod.POST)
    public String register(@RequestParam("username") String username,
                           @RequestParam("password") String password,
                           @RequestParam("firstname") String firstName,
                           @RequestParam("lastname") String lastName,
                           @RequestParam("address") String address,
                           @RequestParam("city") String city,
                           @RequestParam("state") String state,
                           @RequestParam("zipcode") String zipCode,
                           @RequestParam("ssn") String ssn,
                           @RequestParam("hourlyrate") double hourlyRate) {
        try{
            employeeDAO.saveRep(firstName, lastName, address, city, state, zipCode, username, password, ssn, hourlyRate);
        }catch(Exception e) {
            e.printStackTrace();
            return "redirect:/rep/register?error=TRUE";
        }
        return "redirect:/";
    }
}
