package app.controllers;

import app.dao.EmployeeDAO;
import app.entities.CustomUser;
import app.entities.Employee;
import app.entities.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    EmployeeDAO employeeDAO;

    @RequestMapping("/all")
    public String getAll(Model model) {
        List<Employee> employees = employeeDAO.getAllEmployees();
        model.addAttribute("employees", employees);
        return "employees/all";
    }

    @RequestMapping("/self")
    public String getSelf() {
        CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Person person = user.getPerson();
        Employee e = employeeDAO.getEmployee(person);
        return "redirect:/employee/view/" + e.getSsn();
    }

    @RequestMapping("/view/{ssn}")
    public String view(@PathVariable("ssn") String ssn, Model model) {
        CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Person person = user.getPerson();
        Employee employee = null;
        try {
            employee = employeeDAO.getEmployee(ssn);
        }catch(Exception e) {
            e.printStackTrace();
            model.addAttribute("error", "No such employee exists");
            return "redirect:/showError";
        }
        if(!user.hasAuthority("CUSTOMER")) {
            model.addAttribute("employee", employee);
            return "employees/view";
        }else {
            model.addAttribute("error", "Cannot access this page");
            return "redirect:/showError";
        }

    }

    @RequestMapping("/view/{ssn}/update")
    public String update(@PathVariable("ssn") String ssn, Model model) {
        CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Person person = user.getPerson();
        Employee employee = null;
        try {
            employee = employeeDAO.getEmployee(ssn);
        }catch(Exception e) {
            e.printStackTrace();
            model.addAttribute("error", "No such employee exists");
            return "redirect:/showError";
        }
        if(user.hasAuthority("MANAGER") || person.getId() == employee.getId()) {
            model.addAttribute("employee", employee);
            return "employees/update";
        }else {
            model.addAttribute("error", "Cannot access this page");
            return "redirect:/showError";
        }

    }

    @RequestMapping(value="/view/{ssn}/update", method = RequestMethod.POST)
    public String update(@PathVariable("ssn") String ssn,
                           @RequestParam("firstname") String firstname,
                           @RequestParam("lastname") String lastName,
                           @RequestParam("address") String address,
                           @RequestParam("city") String city,
                           @RequestParam("state") String state,
                           @RequestParam("zipcode") String zipCode,
                           @RequestParam(value="hourlyrate", required=false) Double hourlyRate,
                           Model model) {

        CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Person person = user.getPerson();
        Employee employee = null;
        try {
            employee = employeeDAO.getEmployee(ssn);
        }catch(Exception e) {
            e.printStackTrace();
            model.addAttribute("error", "no such employee exists");
            return "redirect:/showError";
        }
        setPersonDetails(employee, firstname, lastName, address, city, state, zipCode);
        if(hourlyRate != null)
            employee.setHourlyRate(hourlyRate);
        employeeDAO.update(employee);
        return "redirect:/employee/view/" + ssn;
    }

    private void setPersonDetails(Person employee, String ...args) {
        employee.setFirstName(args[0]);
        employee.setLastName(args[1]);
        employee.setAddress(args[2]);
        employee.setCity(args[3]);
        employee.setState(args[4]);
    }
}
