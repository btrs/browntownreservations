package app.controllers;

import app.dao.CustomerDAO;
import app.dao.ReservationDAO;
import app.entities.CustomUser;
import app.entities.Person;
import app.entities.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/reservation")
public class ReservationController {

    @Autowired
    private ReservationDAO reservationDAO;

    @RequestMapping("/{resrNo}")
    public String getReservation(@PathVariable("resrNo") int resrNo,
                                 RedirectAttributes redAtt,
                                 Model model) {
        CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Person person = user.getPerson();
        Reservation res = null;
        try {
            res = reservationDAO.getReservation(resrNo);
        }catch(EmptyResultDataAccessException e) {
            redAtt.addFlashAttribute("error", "No such reservation");
            return "redirect:/showError";
        }
        if(user.getPerson().getId() == res.getCustomer().getId() || !user.hasAuthority("CUSTOMER")) {
            model.addAttribute("res", res);
            return "reservations/view";
        }
        redAtt.addFlashAttribute("error", "You cannot view another customer's reservations");
        return "redirect:/showError";
    }
}
