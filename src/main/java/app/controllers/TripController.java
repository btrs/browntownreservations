package app.controllers;

import app.dao.TripDAO;
import app.entities.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/trip")
public class TripController {

    @Autowired
    TripDAO tripDAO;

    @RequestMapping("/{tripID}")
    public String getTrip(@PathVariable("tripID") int tripID,
                          RedirectAttributes redAtt,
                          Model model) {
        try {
            Trip t = tripDAO.getTrip(tripID);
            model.addAttribute("trip", t);
            return "trips/view";
        }catch (EmptyResultDataAccessException e) {
            redAtt.addFlashAttribute("error", "No such trip exists");
            return "redirect:/showError";
        }
    }
}
