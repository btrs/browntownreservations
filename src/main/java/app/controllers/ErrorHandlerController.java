package app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorHandlerController {

    @RequestMapping("/showError")
    public String showError() {
        return "error";
    }
}
