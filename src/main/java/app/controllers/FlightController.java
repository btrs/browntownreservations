package app.controllers;

import app.dao.AirlineDAO;
import app.dao.FlightDAO;
import app.entities.Airline;
import app.entities.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/flight")
public class FlightController {

    @Autowired
    AirlineDAO airlineDAO;

    @Autowired
    private FlightDAO flightDAO;

    @RequestMapping("/{airlineId}")
    public String getAirline(@PathVariable("airlineId") String airlineID,
                             RedirectAttributes redAtt,
                             Model model) {
        try{
            Airline a = airlineDAO.getAirline(airlineID);
            model.addAttribute("airline", a);
            return "airlines/view";
        }catch(Exception e) {
            redAtt.addFlashAttribute("error", "No such airline found");
            return "redirect:/showError";
        }
    }

    @RequestMapping("/{airlineId}/{flightNo}")
    public String getFlight(@PathVariable("airlineId") String airlineID,
                            @PathVariable("flightNo") int flightNo,
                            RedirectAttributes redAtt,
                            Model model) {
        try{
            Flight flight = flightDAO.getFlight(airlineID, flightNo);
            model.addAttribute("flight", flight);
            return "flights/view";
        }catch(Exception e) {
            redAtt.addFlashAttribute("error", "No such flight found");
            return "redirect:/showError";
        }
    }

    @RequestMapping("/flights")
    public String queryFlight() {
        return "flights/flights";
    }

}
