package app.entities.fares;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public class Fare {

    private Map<String, Map<String, Double>> fareMap = new HashMap<>();

    public void addFare(String fareType, String fareClass, Double fareValue) {
        if(fareMap.get(fareType) == null)
            fareMap.put(fareType, new HashMap<>());
        fareMap.get(fareType).put(fareClass, fareValue);
    }

    public Double getPrice(String fareType, String fareClass) {
        return fareMap.get(fareType).get(fareClass);
    }

    public Map<String, Map<String, Double>> getFareMap() {
        return fareMap;
    }
}
