package app.entities;

import java.sql.Timestamp;
import java.util.List;

public class Reservation {

    private int resrNo;
    private Timestamp resrDate;
    private double bookingFee;
    private double totalFare;
    private Employee employee;
    private Customer customer;
    private List<Leg> legs;

    public int getResrNo() {
        return resrNo;
    }

    public void setResrNo(int resrNo) {
        this.resrNo = resrNo;
    }

    public Timestamp getResrDate() {
        return resrDate;
    }

    public void setResrDate(Timestamp resrDate) {
        this.resrDate = resrDate;
    }

    public double getBookingFee() {
        return bookingFee;
    }

    public void setBookingFee(double bookingFee) {
        this.bookingFee = bookingFee;
    }

    public double getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(double totalFare) {
        this.totalFare = totalFare;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Leg> getLegs() {
        return legs;
    }

    public void setLegs(List<Leg> legs) {
        this.legs = legs;
    }
}
