package app.entities;

import app.entities.fares.Fare;

public class Flight {

    private Airline airline;
    private int flightNo;
    private int noOfSeats;
    private Fare fare;

    public Flight(Airline airline, int flightNo) {
        this.setAirline(airline);
        this.setFlightNo(flightNo);
    }

    public Airline getAirline() {
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;
    }

    public int getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(int flightNo) {
        this.flightNo = flightNo;
    }

    public int getNoOfSeats() {
        return noOfSeats;
    }

    public void setNoOfSeats(int noOfSeats) {
        this.noOfSeats = noOfSeats;
    }

    public Fare getFare() {
        return fare;
    }

    public void setFare(Fare fare) {
        this.fare = fare;
    }
}
