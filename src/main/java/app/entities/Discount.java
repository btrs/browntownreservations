package app.entities;

public class Discount {

    public int days;
    public double percent;

    public Discount(int days, double percent) {
        this.days = days;
        this.percent = percent;
    }
}
