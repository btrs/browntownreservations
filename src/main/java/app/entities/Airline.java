package app.entities;

import java.util.ArrayList;
import java.util.List;

public class Airline {

    private String id;
    private String name;
    private List<Discount> discounts;

    public Airline(String id, String name) {
        this.setId(id);
        this.setName(name);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Discount> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(List<Discount> discounts) {
        this.discounts = discounts;
    }

    public double getDiscount(int days) {
        for(Discount d : discounts) {
            if(days >= d.days) {
                return d.percent;
            }
        }
        return 0;
    }

}
