package app.entities;

public class ReservationDetails {

    private int resrNo;
    private int accountNo;

    public int getResrNo() {
        return resrNo;
    }

    public void setResrNo(int resrNo) {
        this.resrNo = resrNo;
    }

    public int getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(int accountNo) {
        this.accountNo = accountNo;
    }
}
