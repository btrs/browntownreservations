package app.entities;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class CustomUser extends User {

    private Person person;

    public CustomUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public void setPerson(Person p) {
        this.person = p;
    }

    public Person getPerson() {
        return person;
    }

    public boolean hasAuthority(String authority) {
        authority = "ROLE_" + authority;
        for(GrantedAuthority sga : this.getAuthorities()) {
            if(authority.equals(sga.toString()))
                return true;
        }
        return false;
    }

}
