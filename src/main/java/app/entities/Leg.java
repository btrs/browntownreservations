package app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Leg {

    private int TripID;
    private Integer legNo;
    private int resrNo;
    private String deptAirportID;
    private String arrAirportID;
    private Timestamp deptTime;
    private Timestamp arrTime;


    public int getTripID() {
        return TripID;
    }

    public void setTripID(int tripID) {
        TripID = tripID;
    }

    public Integer getLegNo() {
        return legNo;
    }

    public void setLegNo(Integer legNo) {
        this.legNo = legNo;
    }

    public int getResrNo() {
        return resrNo;
    }

    public void setResrNo(int resrNo) {
        this.resrNo = resrNo;
    }

    public String getDeptAirportID() {
        return deptAirportID;
    }

    public void setDeptAirportID(String deptAirportID) {
        this.deptAirportID = deptAirportID;
    }

    public String getArrAirportID() {
        return arrAirportID;
    }

    public void setArrAirportID(String arrAirportID) {
        this.arrAirportID = arrAirportID;
    }

    public Timestamp getDeptTime() {
        return deptTime;
    }

    public void setDeptTime(Timestamp deptTime) {
        this.deptTime = deptTime;
    }

    public Timestamp getArrTime() {
        return arrTime;
    }

    public void setArrTime(Timestamp arrTime) {
        this.arrTime = arrTime;
    }
}
