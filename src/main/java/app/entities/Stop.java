package app.entities;

import java.sql.Date;
import java.sql.Timestamp;

public class Stop {

    private Timestamp arrTime;
    private Timestamp depTime;
    private int stopNo;
    private String airportID;
    private Timestamp actualArrTime;
    private Timestamp actualDepTime;

    public Timestamp getArrTime() {
        return arrTime;
    }

    public void setArrTime(Timestamp arrTime) {
        this.arrTime = arrTime;
    }

    public Timestamp getDepTime() {
        return depTime;
    }

    public void setDepTime(Timestamp depTime) {
        this.depTime = depTime;
    }

    public int getStopNo() {
        return stopNo;
    }

    public void setStopNo(int stopNo) {
        this.stopNo = stopNo;
    }

    public String getAirportID() {
        return airportID;
    }

    public void setAirportID(String airportID) {
        this.airportID = airportID;
    }

    public Timestamp getActualArrTime() {
        return actualArrTime;
    }

    public void setActualArrTime(Timestamp actualArrTime) {
        this.actualArrTime = actualArrTime;
    }

    public Timestamp getActualDepTime() {
        return actualDepTime;
    }

    public void setActualDepTime(Timestamp actualDepTime) {
        this.actualDepTime = actualDepTime;
    }
}
