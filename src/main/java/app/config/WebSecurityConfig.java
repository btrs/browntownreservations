package app.config;

import app.dao.CustomUserDAO;
import app.dao.PersonDAO;
import app.entities.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.templateresolver.TemplateResolver;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private CustomUserDAO customUserDAO;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests().antMatchers("/customer/all").hasAnyRole("REP", "MANAGER").and()
                .authorizeRequests().antMatchers("/customer/register").permitAll().and()
                .authorizeRequests().antMatchers("/customer/view/**").hasAnyRole("CUSTOMER", "REP", "MANAGER").and()
                .authorizeRequests().antMatchers("/customer/reservations/new").hasAnyRole("CUSTOMER", "REP").and()
                .authorizeRequests().antMatchers("/customer/**").hasRole("CUSTOMER").and()
                .authorizeRequests().antMatchers("/rep/register").hasRole("MANAGER").and()
                .authorizeRequests().antMatchers("/rep/**").hasRole("REP").and()
                .authorizeRequests().antMatchers("/manager").hasRole("MANAGER").and()
                .authorizeRequests().antMatchers("/employee/**").hasAnyRole("REP", "MANAGER").and()
                .authorizeRequests().antMatchers("/reservation/**").authenticated().and()
                .authorizeRequests().anyRequest().permitAll().and()
                .csrf().disable()
                .formLogin().loginPage("/login").failureUrl("/login?error=TRUE").and()
                .logout().logoutSuccessUrl("/").and()
                .httpBasic();

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // TODO - add BCrypPasswordEncoder later
        auth.userDetailsService(customUserDAO);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
