USE manasingh;

-- drop the procedures if they exist
DROP PROCEDURE IF EXISTS insert_customer;
DROP PROCEDURE IF EXISTS insert_passenger;
DROP PROCEDURE IF EXISTS insert_manager;
DROP PROCEDURE IF EXISTS insert_rep;
DROP PROCEDURE IF EXISTS update_customer;
DROP PROCEDURE IF EXISTS update_employee;
DROP PROCEDURE IF EXISTS insert_resvs_and_legs;

-- create a procedure to insert a customer (and create the person and user)
DELIMITER //
CREATE PROCEDURE insert_customer(
    IN FirstName VARCHAR(20),
    IN LastName VARCHAR(20),
    IN Address VARCHAR(100),
    IN City VARCHAR(50),
    IN State VARCHAR(50),
    IN ZipCode VARCHAR(50),
    IN Username VARCHAR(20),
    IN Password VARCHAR(20),
    IN CreditCardNo CHAR(16),
    IN Email VARCHAR(50)
)
BEGIN
    DECLARE person_id INTEGER;
    DECLARE account_no INTEGER;
    SET person_id = 0;
    SET account_no = 0;
    INSERT INTO Person(FirstName, LastName, Address, City, State, ZipCode) VALUES (FirstName, LastName, Address, City, State, ZipCode);
    SELECT LAST_INSERT_ID() INTO person_id;
    INSERT INTO User(Username, Password, PersonId) VALUES (Username, Password, person_id);
    INSERT INTO Authorities(Username, Authority) VALUES (Username, 'ROLE_CUSTOMER');
    INSERT INTO Customer(Id, CreditCardNo, Email) VALUES (person_id, CreditCardNo, Email);
    SELECT LAST_INSERT_ID() INTO account_no;
    INSERT INTO Passenger(Id, AccountNo) VALUES (person_id, account_no);
END //
DELIMITER ;

-- insert a passenger (and create a person)
DELIMITER //
CREATE PROCEDURE insert_passenger(
    IN FirstName VARCHAR(20),
    IN LastName VARCHAR(20),
    IN Address VARCHAR(100),
    IN City VARCHAR(50),
    IN State VARCHAR(50),
    IN ZipCode VARCHAR(50),
    IN AccountNo INTEGER
)
BEGIN
    DECLARE person_id INTEGER;
    SET person_id = 0;
    INSERT INTO Person(FirstName, LastName, Address, City, State, ZipCode) VALUES (FirstName, LastName, Address, City, State, ZipCode);
    SELECT LAST_INSERT_ID() INTO person_id;
    INSERT INTO Passenger(Id, AccountNo) VALUES (person_id, AccountNo);
END //
DELIMITER ;

-- insert an employee (and create a person)
DELIMITER //
CREATE PROCEDURE insert_manager(
    IN FirstName VARCHAR(20),
    IN LastName VARCHAR(20),
    IN Address VARCHAR(100),
    IN City VARCHAR(50),
    IN State VARCHAR(50),
    IN ZipCode VARCHAR(50),
    IN Username VARCHAR(20),
    IN Password VARCHAR(20),
    IN SSN INTEGER,
    IN HourlyRate NUMERIC(10, 2)
)
BEGIN
    DECLARE person_id INTEGER;
    SET person_id = 0;
    INSERT INTO Person(FirstName, LastName, Address, City, State, ZipCode) VALUES (FirstName, LastName, Address, City, State, ZipCode);
    SELECT LAST_INSERT_ID() INTO person_id;
    INSERT INTO User(Username, Password, PersonID) VALUES (Username, Password, person_id);
    INSERT INTO Authorities(Username, Authority) VALUES (Username, 'ROLE_MANAGER');
    INSERT INTO Employee(Id, SSN, HourlyRate) VALUES (person_id, SSN, HourlyRate);
END //
DELIMITER ;

-- insert an customer rep (and create a person)
DELIMITER //
CREATE PROCEDURE insert_rep(
    IN FirstName VARCHAR(20),
    IN LastName VARCHAR(20),
    IN Address VARCHAR(100),
    IN City VARCHAR(50),
    IN State VARCHAR(50),
    IN ZipCode VARCHAR(50),
    IN Username VARCHAR(20),
    IN Password VARCHAR(20),
    IN SSN VARCHAR(20),
    IN HourlyRate NUMERIC(10, 2)
)
BEGIN
    DECLARE person_id INTEGER;
    SET person_id = 0;
    INSERT INTO Person(FirstName, LastName, Address, City, State, ZipCode) VALUES (FirstName, LastName, Address, City, State, ZipCode);
    SELECT LAST_INSERT_ID() INTO person_id;
    INSERT INTO User(Username, Password, PersonID) VALUES (Username, Password, person_id);
    INSERT INTO Authorities(Username, Authority) VALUES (Username, 'ROLE_REP');
    INSERT INTO Employee(Id, SSN, HourlyRate) VALUES (person_id, SSN, HourlyRate);
END //
DELIMITER ;

-- update the information of a customer
DELIMITER //
CREATE PROCEDURE update_customer(
    IN FirstName VARCHAR(20),
    IN LastName VARCHAR(20),
    IN Address VARCHAR(100),
    IN City VARCHAR(50),
    IN State VARCHAR(50),
    IN ZipCode VARCHAR(50),
    IN accNo INTEGER,
    IN CreditCardNo CHAR(16),
    IN Email VARCHAR(50)
)
BEGIN
    DECLARE person_id INTEGER;
    SELECT Id FROM Customer WHERE AccountNo = accNo INTO person_id;
    UPDATE Customer SET CreditCardNo = CreditCardNo, Email = Email WHERE AccountNo = accNo;
    UPDATE Person SET FirstName = FirstName, LastName = LastName, Address = Address, City = City, State = State, ZipCode = ZipCode WHERE Id = person_id;
END //
DELIMITER ;

-- update the information of an employee
DELIMITER //
CREATE PROCEDURE update_employee(
    IN FirstName VARCHAR(20),
    IN LastName VARCHAR(20),
    IN Address VARCHAR(100),
    IN City VARCHAR(50),
    IN State VARCHAR(50),
    IN ZipCode VARCHAR(50),
    IN SocialSec VARCHAR(20),
    IN HourlyRate NUMERIC(10, 2)
)
BEGIN
    DECLARE person_id INTEGER;
    SELECT Id FROM Employee WHERE SSN = SocialSec INTO person_id;
    UPDATE Employee SET HourlyRate = HourlyRate WHERE SSN = SocialSec;
    UPDATE Person SET FirstName = FirstName, LastName = LastName, Address = Address, City = City, State = State, ZipCode = ZipCode WHERE Id = person_id;
END //
DELIMITER ;

-- insert a Reservation and the appropriate legs and also insert into Includes
DELIMITER //
CREATE PROCEDURE insert_resvs_and_legs(
    IN accountNumber INTEGER,
    IN flightTripID INTEGER,
    IN startStop INTEGER,
    IN endStop INTEGER,
    IN farePrice NUMERIC(10, 2),
    IN bookingPrice NUMERIC(10, 2),
    IN repSocialSec INTEGER
)
BEGIN
    DECLARE res_no INTEGER;
    DECLARE counter INTEGER;
    DECLARE length INTEGER;
    DECLARE leg_no INTEGER;
    DECLARE stop_counter INTEGER;
    INSERT INTO Reservation(BookingFee, TotalFare, RepSSN, AccountNo) VALUES (bookingPrice, farePrice, repSocialSec, accountNumber);
    SET res_no = 0;
    SELECT LAST_INSERT_ID() INTO res_no;
    SET counter = 0;
    SET length = endStop - startStop;
    SET leg_no = 1;
    SET stop_counter = startStop;
    WHILE counter < length DO
        SELECT ArrTime, DepTime, AirportID INTO @s1_at, @s1_dt, @s1_ai FROM StopsAt WHERE TripID = flightTripID AND StopNo = stop_counter;
        SELECT ArrTime, DepTime, AirportID INTO @s2_at, @s2_dt, @s2_ai FROM StopsAt WHERE TripID = flightTripID AND StopNo = (stop_counter + 1);
        INSERT INTO Leg(TripID, LegNo, ResrNo, DestArrTime, DepTime, ArrAirport, DepAirport) VALUES (flightTripID, leg_no, res_no, @s2_at, @s1_dt, @s2_ai, @s1_ai);
        SET leg_no = leg_no + 1;
        SET stop_counter = stop_counter + 1;
        SET counter = counter + 1;
    END WHILE;
END //
DELIMITER ;
