USE manasingh;

-- this script assumes that the InsertBasicPeople.sql has been run already

SET foreign_key_checks = 0;
DELETE FROM CustomerPreferences;
DELETE FROM Reservation;
DELETE FROM ReservationPassenger;
DELETE FROM Leg;
SET foreign_key_checks = 1;

-- insert a reservations for a customer
INSERT INTO Reservation(BookingFee, TotalFare, AccountNo) VALUES (10.00, 1200.00, 3);
INSERT INTO Reservation(BookingFee, TotalFare, AccountNo) VALUES (10.00, 500.00, 2);
INSERT INTO Reservation(BookingFee, TotalFare, AccountNo) VALUES (10.00, 3333.33, 4);



INSERT INTO Leg(TripID, LegNo, ResrNo, DestArrTime, DepTime, ArrAirport, DepAirport)
VALUES (1, 1, 1, '2011-01-05 17:00:00' , '2011-01-05 11:00:00', 'LAI', 'LGA');

INSERT INTO Leg(TripID, LegNo, ResrNo, DestArrTime, DepTime, ArrAirport, DepAirport)
VALUES (1, 2, 1,'2011-01-06 07:30:00', '2011-01-05 19:00:00', 'TKI', 'LAI');

INSERT INTO Leg(TripID, LegNo, ResrNo, DestArrTime, DepTime, ArrAirport, DepAirport)
VALUES (2, 1, 2, '2011-01-11 05:00:00', '2011-01-10 22:30:00', 'LDH', 'LGI');

INSERT INTO Leg(TripID, LegNo, ResrNo, DestArrTime, DepTime, ArrAirport, DepAirport)
VALUES (3, 1, 3, '2011-01-13 23:00:00', '2011-01-13 07:00:00', 'IVI', 'JFK');



-- insert into ReservationPassengers to associate passengers with a reservations
INSERT INTO ReservationPassenger(ResrNo, Id, AccountNo, SeatNo, Class, Meal)
VALUES (1, 5, 3, '33F', 'economy', 'chips');

INSERT INTO ReservationPassenger(ResrNo, Id, AccountNo, SeatNo, Class, Meal)
VALUES (2, 4, 2, '13A', 'first', 'fish and chips');

INSERT INTO ReservationPassenger(ResrNo, Id, AccountNo, SeatNo, Class, Meal)
VALUES (3, 6, 4, '1A', 'first', 'sushi');

-- insert some customer preferences
