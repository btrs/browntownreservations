USE manasingh;

SET foreign_key_checks = 0;
DELETE FROM Airline;
DELETE FROM Flight;
DELETE FROM FlightTrip;
DELETE FROM Airport;
DELETE FROM AdvPurchaseDiscount;
DELETE FROM Fare;
DELETE FROM StopsAt;
DELETE FROM Leg;
SET foreign_key_checks = 1;

-- insert some airlines
INSERT INTO Airline(Id, Name) VALUES ('AB', 'Air Berlin');
INSERT INTO Airline(Id, Name) VALUES ('AJ', 'Air Japan');
INSERT INTO Airline(Id, Name) VALUES ('AM', 'Air Madagascar');
INSERT INTO Airline(Id, Name) VALUES ('AA', 'American Airlines');
INSERT INTO Airline(Id, Name) VALUES ('BA', 'British Airways');
INSERT INTO Airline(Id, Name) VALUES ('DA', 'Delta Airlines');
INSERT INTO Airline(Id, Name) VALUES ('JA', 'JetBlue Airways');
INSERT INTO Airline(Id, Name) VALUES ('LU', 'Lufthansa');
INSERT INTO Airline(Id, Name) VALUES ('SA', 'Southwest Airlines');
INSERT INTO Airline(Id, Name) VALUES ('UA', 'United Airlines');

-- insert some airports
INSERT INTO Airport(ID, Name, City, Country) VALUES ('BET', 'Berlin Tegel', 'Berlin', 'Germany');
INSERT INTO Airport(ID, Name, City, Country) VALUES ('COH', 'Chicago OHare International', 'Chicago', 'United States');
INSERT INTO Airport(ID, Name, City, Country) VALUES ('HJA', 'Hartsfield-Jackson Atlanta Int', 'Atlanta', 'United States');
INSERT INTO Airport(ID, Name, City, Country) VALUES ('IVI', 'Ivato International', 'Antananarivo', 'Madagascar');
INSERT INTO Airport(ID, Name, City, Country) VALUES ('JFK', 'John F. Kennedy International', 'New York', 'United States');
INSERT INTO Airport(ID, Name, City, Country) VALUES ('LGA', 'LaGuardia', 'New York', 'United States');
INSERT INTO Airport(ID, Name, City, Country) VALUES ('LGI', 'Logan International', 'Boston', 'United States');
INSERT INTO Airport(ID, Name, City, Country) VALUES ('LDH', 'London Heathrow', 'London', 'United Kingdom');
INSERT INTO Airport(ID, Name, City, Country) VALUES ('LAI', 'Los Angeles International', 'Los Angeles', 'United States');
INSERT INTO Airport(ID, Name, City, Country) VALUES ('SFI', 'San Francisco International', 'San Francisco', 'United States');
INSERT INTO Airport(ID, Name, City, Country) VALUES ('TKI', 'Tokyo International', 'Tokyo', 'Japan');

-- insert a jet blues flight that occurs MonWedFri
INSERT INTO Flight(AirlineID, FlightNo, NoOfSeats)
VALUES ('AA', 111, 100);


INSERT INTO Flight(AirlineID, FlightNo, NoOfSeats)
VALUES ('JA', 111, 150);


INSERT INTO Flight(AirlineID, FlightNo, NoOfSeats)
VALUES ('AM', 1337, 33);


-- insert all the fares for the airlines
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AA', 111, 'oneway', 'economy', 1200.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AA', 111, 'oneway', 'business', 1500.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AA', 111, 'oneway', 'first', 1700.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AA', 111, 'round', 'economy', 2400.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AA', 111, 'round', 'business', 3000.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AA', 111, 'round', 'first', 3400.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AA', 111, 'hidden', 'economy', 400.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AA', 111, 'hidden', 'business', 600.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AA', 111, 'hidden', 'first', 800.00);

INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('JA', 111, 'oneway', 'economy', 200.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('JA', 111, 'oneway', 'business', 300.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('JA', 111, 'oneway', 'first', 500.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('JA', 111, 'round', 'economy', 400.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('JA', 111, 'round', 'business', 600.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('JA', 111, 'round', 'first', 1000.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('JA', 111, 'hidden', 'economy', 100.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('JA', 111, 'hidden', 'business', 200.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('JA', 111, 'hidden', 'first', 300.00);

INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AM', 1337, 'oneway', 'economy', 2533.33);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AM', 1337, 'oneway', 'business', 2833.33);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AM', 1337, 'oneway', 'first', 3333.33);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AM', 1337, 'round', 'economy', 5066.66);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AM', 1337, 'round', 'business', 5666.666);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AM', 1337, 'round', 'first', 6666.66);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AM', 1337, 'hidden', 'economy', 1000.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AM', 1337, 'hidden', 'business', 1200.00);
INSERT INTO Fare(AirlineID, FlightNo, FareType, Class, Fare) VALUES ('AM', 1337, 'hidden', 'first', 1300.00);

-- insert the appropriate discounts that customers would get for buying at least x days early for JB
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('AB', 3, 5.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('AB', 7, 10.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('AB', 14, 20.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('AB', 21, 25.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('AJ', 14, 15.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('AJ', 7, 7.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('AM', 21, 20.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('AM', 14, 15.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('AM', 7, 5.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('AA', 14, 18.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('AA', 7, 13.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('BA', 14, 12.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('DA', 21, 15.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('JA', 21, 13.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('JA', 7, 8.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('LU', 14, 15.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('SA', 21, 40.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('SA', 14, 25.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('SA', 7, 20.00);
INSERT INTO AdvPurchaseDiscount(AirlineID, Days, Discount) VALUES ('UA', 7, 12.00);

-- create some trips
INSERT INTO FlightTrip(AirlineID, FlightNo) VALUES ('AA', 111);
INSERT INTO FlightTrip(AirlineID, FlightNo) VALUES ('JA', 111);
INSERT INTO FlightTrip(AirlineID, FlightNo) VALUES ('AM', 1337);

-- insert into StopsAt which represents a stop of a flight and gives info on how long that flight stays there
INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2011-01-05 09:00:00', '2011-01-05 11:00:00', 1, 'LGA', 1);

INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2011-01-05 17:00:00', '2011-01-05 19:00:00', 2, 'LAI', 1);

INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2011-01-06 07:30:00', '2011-01-06 10:00:00', 3, 'TKI', 1);

INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2011-01-10 12:00:00', '2011-01-10 14:00:00', 1, 'SFI', 2);

INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2011-01-10 19:30:00', '2011-01-10 22:30:00', 2, 'LGI', 2);

INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2011-01-11 05:00:00', '2011-01-11 08:00:00', 3, 'LDH', 2);

INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2011-01-13 05:00:00', '2011-01-13 07:00:00', 1, 'JFK', 3);

INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2011-01-13 23:00:00', '2011-01-14 03:00:00', 2, 'IVI', 3);

-- insert some more flight data to make things interesting
INSERT INTO FlightTrip(AirlineID, FlightNo) VALUES ('AA', 111);
INSERT INTO FlightTrip(AirlineID, FlightNo) VALUES ('JA', 111);

INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2017-12-8 9:00:00', '2017-12-8 11:00:00', 1, 'LGA', 4);
INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2017-12-9 1:00:00', '2017-12-9 2:00:00', 2, 'LAI', 4);
INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2017-12-9 7:00:00', '2017-12-9 13:00:00', 3, 'TKI', 4);


INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2018-3-4 15:00:00', '2018-3-4 22:00:00', 1, 'SFI', 5);
INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2018-3-5 16:00:00', '2018-3-5 21:00:00', 2, 'JFK', 5);
INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2018-3-7 10:00:00', '2018-3-7 21:00:00', 3, 'LGI', 5);
INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2018-3-8 2:00:00', '2018-3-8 9:00:00', 4, 'LAI', 5);
INSERT INTO StopsAt(ArrTime, DepTime, StopNo, AirportID, TripID)
VALUES ('2018-3-9 11:00:00', '2018-3-9 19:00:00', 5, 'JFK', 5);

-- insert the the legs of the flights which record a trip from point A to B
-- FromStopNo implies that the flight leaves that stop and goes onto the (FromStopNo + 1) stop


