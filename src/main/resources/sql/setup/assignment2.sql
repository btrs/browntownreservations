USE manasingh;

SET foreign_key_checks = 0;
DROP TABLE IF EXISTS Person;
DROP TABLE IF EXISTS User;
DROP TABLE IF EXISTS Authorities;
DROP TABLE IF EXISTS Employee;
DROP TABLE IF EXISTS Customer;
DROP TABLE IF EXISTS Airline;
DROP TABLE IF EXISTS Flight;
DROP TABLE IF EXISTS FlightTrip;
DROP TABLE IF EXISTS Airport;
DROP TABLE IF EXISTS Leg;
DROP TABLE IF EXISTS Fare;
DROP TABLE IF EXISTS CustomerPreferences;
DROP TABLE IF EXISTS AdvPurchaseDiscount;
DROP TABLE IF EXISTS Passenger;
DROP TABLE IF EXISTS Reservation;
DROP TABLE IF EXISTS Includes;
DROP TABLE IF EXISTS ReservationPassenger;
DROP TABLE IF EXISTS Auctions;
DROP TABLE IF EXISTS StopsAt;
SET foreign_key_checks = 1;


/* CREATE Statements */
-- username and password can be null, this means that they are just passengers and not users of the site
CREATE TABLE Person (
    Id INTEGER NOT NULL AUTO_INCREMENT,
    FirstName VARCHAR(20) NOT NULL,
    LastName VARCHAR(20) NOT NULL,
    Address VARCHAR(100) NOT NULL,
    City VARCHAR(50) NOT NULL,
    State VARCHAR(50) NOT NULL,
    ZipCode VARCHAR(50) NOT NULL,
    PRIMARY KEY(Id),
    UNIQUE(FirstName, LastName, Address),
    CHECK(Id > 0)
);

-- @set @personID = 0
-- select last_insert_id() into @person

CREATE TABLE User(
    Username VARCHAR(20) NOT NULL,
    Password VARCHAR(20) NOT NULL,
    PersonID INTEGER NOT NULL,
    PRIMARY KEY(Username, Password)
);

CREATE TABLE Authorities (
    Username VARCHAR(50) NOT NULL,
    Authority VARCHAR(20) NOT NULL,
    PRIMARY KEY (Username, Authority),
    FOREIGN KEY (Username) REFERENCES User(Username)
);

CREATE TABLE Employee (
    Id INTEGER NOT NULL,
    SSN VARCHAR(20) NOT NULL,
    StartDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    HourlyRate NUMERIC(10,2) NOT NULL,
    PRIMARY KEY (SSN),
    FOREIGN KEY (Id) REFERENCES Person(Id) 
        ON DELETE CASCADE 
        ON UPDATE CASCADE,
    UNIQUE(Id),
    CHECK (SSN > 0),
    CHECK (HourlyRate > 0)
);

CREATE TABLE Customer (
    Id INTEGER NOT NULL,
    AccountNo INTEGER NOT NULL AUTO_INCREMENT,
    CreditCardNo CHAR(16) NOT NULL,
    Email VARCHAR(50) NOT NULL,
    CreationDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    Rating INTEGER DEFAULT 0,
    PRIMARY KEY(AccountNo),
    FOREIGN KEY(Id) REFERENCES Person(Id) 
        ON DELETE CASCADE 
        ON UPDATE CASCADE,
    CHECK (Rating >= 0 AND Rating <= 10)
);

CREATE TABLE Airline ( 
    Id CHAR(2),
    Name VARCHAR(100) NOT NULL,
    PRIMARY KEY(Id),
    UNIQUE(Name)
);

CREATE TABLE Flight (
    AirlineID CHAR(2),
    FlightNo INTEGER NOT NULL,
    NoOfSeats INTEGER NOT NULL,
    PRIMARY KEY(AirlineID, FlightNo),
    FOREIGN KEY(AirlineID) REFERENCES Airline(Id),
    CHECK (NoOfSeats > 0)
);

CREATE TABLE FlightTrip (
    TripID INTEGER NOT NULL AUTO_INCREMENT,
    AirlineID CHAR(2) NOT NULL,
    FlightNo INTEGER NOT NULL,
    PRIMARY KEY(TripID),
    FOREIGN KEY(AirlineID, FlightNo) REFERENCES Flight(AirlineID, FlightNo)
);

CREATE TABLE Airport (
    Id CHAR(3),
    Name VARCHAR(100) NOT NULL,
    City VARCHAR(50) NOT NULL,
    Country VARCHAR(50) NOT NULL,
    PRIMARY KEY(Id),
    UNIQUE(Id, Name)
);

CREATE TABLE StopsAt (
    ArrTime DATETIME NOT NULL,
    DepTime DATETIME NOT NULL,
    StopNo INTEGER NOT NULL,
    AirportID CHAR(3),
    ActualArrTime DATETIME,
    ActualDepTime DATETIME,
    TripID INTEGER,
    PRIMARY KEY (StopNo, ArrTime, DepTime, TripID),
    FOREIGN KEY (AirportID) REFERENCES Airport(Id),
    FOREIGN KEY (TripID) REFERENCES FlightTrip(TripID),
    CHECK (DepTime > ArrTime),
    CHECK (StopNo > 0),
    CHECK (DeptAirportID != ArrAirportID)
);

CREATE TABLE Fare (
    AirlineID CHAR(2) NOT NULL,
    FlightNo INTEGER NOT NULL,
    FareType VARCHAR(20) NOT NULL,
    Class VARCHAR(20) NOT NULL,
    Fare NUMERIC(10, 2) NOT NULL,
    PRIMARY KEY(AirlineID, FlightNo, FareType, Class),
    FOREIGN KEY(AirlineID, FlightNo) REFERENCES Flight(AirlineID, FlightNo),
    CHECK (Fare > 0)
);

CREATE TABLE CustomerPreferences (
    AccountNo INTEGER NOT NULL,
    Preference VARCHAR(50) NOT NULL,
    PRIMARY KEY(AccountNo, Preference),
    FOREIGN KEY(AccountNo) REFERENCES Customer(AccountNo)
);

CREATE TABLE AdvPurchaseDiscount (
    AirlineID CHAR(2),
    Days INTEGER NOT NULL,
    Discount NUMERIC(10,2) NOT NULL,
    PRIMARY KEY(AirlineID, Days),
    FOREIGN KEY(AirlineID) REFERENCES Airline(Id),
    CHECK (Days > 0),
    CHECK (DiscountRate > 0 AND DiscountRate < 100)
);

CREATE TABLE Passenger (
    Id INTEGER NOT NULL,
    AccountNo INTEGER NOT NULL,
    PRIMARY KEY (Id, AccountNo),
    FOREIGN KEY (Id) REFERENCES Person(Id),
    FOREIGN KEY (AccountNo) REFERENCES Customer(AccountNo)
);

CREATE TABLE Reservation (
    ResrNo INTEGER NOT NULL AUTO_INCREMENT,
    ResrDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    BookingFee NUMERIC(10,2) NOT NULL,
    TotalFare NUMERIC(10,2) NOT NULL,
    RepSSN VARCHAR(20),
    AccountNo INTEGER NOT NULL,
    PRIMARY KEY (ResrNo),
    FOREIGN KEY (RepSSN) REFERENCES Employee(SSN),
    FOREIGN KEY (AccountNo) REFERENCES Customer(AccountNo),
    CHECK (ResrNo > 0),
    CHECK (BookingFee >= 0)
);

CREATE TABLE Leg (
    TripID INTEGER NOT NULL,
    LegNo INTEGER NOT NULL,
    ResrNo INTEGER NOT NULL,
    DestArrTime DATETIME NOT NULL,
    DepTime DATETIME NOT NULL,
    ArrAirport CHAR(3) NOT NULL,
    DepAirport CHAR(3) NOT NULL,
    PRIMARY KEY(TripID, LegNo, ResrNo),
    FOREIGN KEY (ResrNo) REFERENCES Reservation(ResrNo),
    FOREIGN KEY (TripID) REFERENCES FlightTrip(TripID),
    CHECK (LegNo > 0)
);


CREATE TABLE ReservationPassenger (
    ResrNo INTEGER,
    Id INTEGER,
    AccountNo INTEGER,
    SeatNo CHAR(5) NOT NULL,
    Class VARCHAR(20) NOT NULL,
    Meal VARCHAR(50),
    PRIMARY KEY (ResrNo, Id, AccountNo),
    FOREIGN KEY (ResrNo) REFERENCES Reservation (ResrNo),
    FOREIGN KEY (Id, AccountNo) REFERENCES Passenger (Id, AccountNo)
);

CREATE TABLE Auctions (
    AccountNo INTEGER,
    AirlineID CHAR(2),
    FlightNo INTEGER,
    Class VARCHAR(20),
    Date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    NYOP NUMERIC(10,2) NOT NULL,
    PRIMARY KEY (AccountNo, AirlineID, FlightNo, Class, Date),
    FOREIGN KEY (AccountNo) REFERENCES Customer (AccountNo),
    FOREIGN KEY (AirlineID, FlightNo) REFERENCES Flight(AirlineID, FlightNo),
    CHECK (NYOP > 0)
);

