To set up the database appropriately, you want run the setup scripts in this order:
    1 - StoredProcedures.sql
    2 - assignment2.sql
    3 - InsertBasicPeople.sql
    4 - InsertBasicAirlines.sql
    5 - InsertBasicReservations.sql

With that, the database should now have all the sample demo data inside it.
