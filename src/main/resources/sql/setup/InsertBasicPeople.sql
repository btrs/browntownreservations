USE manasingh;

SET foreign_key_checks = 0;
delete from Person;
delete from Employee;
delete from Customer;
delete from Passenger;
SET foreign_key_checks = 1;

-- insesrt some person rows
INSERT INTO Person(FirstName, LastName, Address, City, State, ZipCode)
VALUES ('Manan', 'Singh', '69 West 5th Street', 'Deer Park', 'NY', '11729');

INSERT INTO Person(FirstName, LastName, Address, City, State, ZipCode)
VALUES ('Manjur', 'Khan', 'Unknown', 'Unknown', 'NY', 'Unknown');

INSERT INTO Person(FirstName, LastName, Address, City, State, ZipCode)
VALUES ('Scott', 'Smolka', 'Unknown', 'Unknown', 'NY', 'Unknown');

INSERT INTO Person(FirstName, LastName, Address, City, State, ZipCode)
VALUES ('Jane', 'Smith', '100 Nicolls Rd', 'Stony Brook', 'NY', '11790');

INSERT INTO Person(FirstName, LastName, Address, City, State, ZipCode)
VALUES ('John', 'Doe', '123 N Fake Street', 'New York', 'NY', '10001');

INSERT INTO Person(FirstName, LastName, Address, City, State, ZipCode)
VALUES ('Rick', 'Astley', '1337 Internet Lane', 'Los Angeles', 'CA', '90001');

INSERT INTO Person(FirstName, LastName, Address, City, State, ZipCode)
VALUES ('Kirat', 'Singh', '69 West 5th Street', 'Deer Park', 'NY', '11729');

-- make users
INSERT INTO User(Username, Password, PersonID) VALUES ('manasingh', 'password', 1);
INSERT INTO User(Username, Password, PersonID) VALUES ('mankhan', 'password', 2);
INSERT INTO User(Username, Password, PersonID) VALUES ('scosmolka', 'password', 3);
INSERT INTO User(Username, Password, PersonID) VALUES ('jsmith', 'password', 4);
INSERT INTO User(Username, Password, PersonID) VALUES ('jdoe', 'password', 5);
INSERT INTO User(Username, Password, PersonID) VALUES ('rastly', 'password', 6);

-- make a customer
INSERT INTO Customer(Id, CreditCardNo, Email) VALUES (1, '1234567890123456', 'manan.singh@stonybrook.edu');
INSERT INTO Customer(Id, CreditCardNo, Email) VALUES (4, '3643574357546864', 'awesomejane@ftw.com');
INSERT INTO Customer(Id, CreditCardNo, Email) VALUES (5, '7592658302503759', 'jdoe@woot.com');
INSERT INTO Customer(Id, CreditCardNo, Email) VALUES (6, '2048574940375620', 'rickroller@rolld.com');

-- insert the passengers (all customers are by default passengers)
INSERT INTO Passenger(Id, AccountNo) VALUES (1, 1);
INSERT INTO Passenger(Id, AccountNo) VALUES (7, 1); -- this one is just a passenger, not a customer
INSERT INTO Passenger(Id, AccountNo) VALUES (4, 2);
INSERT INTO Passenger(Id, AccountNo) VALUES (5, 3);
INSERT INTO Passenger(Id, AccountNo) VALUES (6, 4);

-- make two employee
INSERT INTO Employee(Id, SSN, HourlyRate) VALUES (3, 21, 12.00);
INSERT INTO Employee(Id, SSN, HourlyRate) VALUES (2, 31, 12.00);

-- assign roles of customer rep and manager to employees and customer to the customer
INSERT INTO Authorities(Username, Authority) VALUES ('scosmolka', 'ROLE_MANAGER');
INSERT INTO Authorities(Username, Authority) VALUES ('mankhan', 'ROLE_REP');
INSERT INTO Authorities(Username, Authority) VALUES ('manasingh', 'ROLE_CUSTOMER');
INSERT INTO Authorities(Username, Authority) VALUES ('jsmith', 'ROLE_CUSTOMER');
INSERT INTO Authorities(Username, Authority) VALUES ('jdoe', 'ROLE_CUSTOMER');
INSERT INTO Authorities(Username, Authority) VALUES ('rastly', 'ROLE_CUSTOMER');

