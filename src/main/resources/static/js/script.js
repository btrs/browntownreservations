$(document).ready(function() {
    var date_input=$('.date');
    var options = {
        format: "yyyy-mm-dd",
        minDate: 0,
        todayHighlight: true,
        autoclose: true
    };
    date_input.datepicker(options);

    $('#flight_search_submit').click(function (event) {
        event.preventDefault();
        var travel_path = '/flightTrip/' + $("#travel-preference").val();
        console.log(travel_path);
        console.log($('#flight_search').serialize());
        if($('#search-dep').val().toUpperCase() == $('#search-arr').val().toUpperCase()) {
            alert("From and to are the same");
            return;
        }
;       $.ajax({
            url: travel_path,
            type: 'get',
            data: {'start' : $('#search-dep').val().toUpperCase() , 'stop' : $('#search-arr').val().toUpperCase(),
                'dep' : $('#saerch-start').val(), 'arr' : $('#saerch-stop').val()},
            success: function(data) {
                console.log(data);
                $('#search_result').empty();
                $('#search_result_h').html('<h4>Result for '+ $("#travel-preference").val() +' From: '+ $('#search-dep').val().toUpperCase() +' To: ' + $('#search-arr').val().toUpperCase() + ' Start Date: '+ $('#saerch-start').val() +' End Date: ' + $('#saerch-stop').val() +' </h4>');
                for(var i = 0; i < data.length; i++) {
                    var path_direction = window.location.origin + '/trip/' + data[i];
                    var to_append = '<a class=\"text-center row flight-search-result-a\" href=' + path_direction + '\\>Flight ' + (i + 1) + '</a>';
                    console.log(to_append);
                    $('#search_result').append(to_append);
                    $('#search_result').append('<div class="spacer-row"></div>');
                }
                if(data.error) {
                    $('#search_result').append('<p class=\"alert alert-warning"> Search does not exist</p>');
                }
            }
        });
    });

    $('#get_list_of_most_active_flights').click(function (event) {
        event.preventDefault();
        var travel_path = window.location.origin + '/transact/getListOfMostActiveFlights';
        console.log(travel_path);
        $.ajax({
            url: travel_path,
            type: 'get',
            success: function(data) {
                console.log(data);
                $('#manager-table-id').empty();
                $('#manager-table-id').html('List of Most Active Flights');
                $('#manager-table-head').empty();
                $('#manager-table-body').empty();
                // for(var i = 0; i < data.length; i++) {
                //
                // }
                print_data_to_table(data);
                if(data.error || data == null) {
                    $('#manager-table-id').append('<p class=\"alert alert-warning"> Search does not exist</p>');
                }
            }
        });
    });

    $('#get_rep_with_most_rev').click(function (event) {
        event.preventDefault();
        var travel_path = window.location.origin + '/transact/getRepWithMostRev';
        console.log(travel_path);
        $.ajax({
            url: travel_path,
            type: 'get',
            success: function(data) {
                console.log(data);
                $('#manager-table-id').empty();
                $('#manager-table-id').html('Get Representative With Most Revenue');
                $('#manager-table-head').empty();
                $('#manager-table-body').empty();

                print_data_to_table(data);
                if(data.error || data == null) {
                    $('#manager-table-id').append('<p class=\"alert alert-warning"> Search does not exist</p>');
                }
            }
        });
    });

    $('#get_cus_with_most_rev').click(function (event) {
        event.preventDefault();
        var travel_path = window.location.origin + '/transact/getCusWithMostRevenue';
        console.log(travel_path);
        $.ajax({
            url: travel_path,
            type: 'get',
            success: function(data) {
                console.log(data);
                $('#manager-table-id').empty();
                $('#manager-table-id').html('Get Customer With Most Revenue');
                $('#manager-table-head').empty();
                $('#manager-table-body').empty();

                print_data_to_table(data);
                if(data.error || data == null) {
                    $('#manager-table-id').append('<p class=\"alert alert-warning"> Search does not exist</p>');
                }
            }
        });
    });

    $('#get_resr_by_trip').click(function (event) {
        event.preventDefault();
        var trip_id = $('#tripID').val().trim();
        if(trip_id == "" || trip_id == null) {
            alert("Trip ID is invalid");
            return;
        }
        var travel_path = window.location.origin + '/transact/getListOfReserByTrip';
        console.log(travel_path);
        $.ajax({
            url: travel_path,
            type: 'get',
            data: {'tripID': trip_id},
            success: function(data) {
                console.log(data);
                $('#manager-table-id').empty();
                $('#manager-table-id').html('Get List of Reservation by Trip ID');
                $('#manager-table-head').empty();
                $('#manager-table-body').empty();

                print_data_to_table(data);
                if(data.error || data == null) {
                    $('#manager-table-id').append('<p class=\"alert alert-warning"> Search does not exist</p>');
                }
            }
        });
    });

    $('#get_resr_by_customer').click(function (event) {
        event.preventDefault();
        var first_name = $('#first_name').val().trim();
        var last_name = $('#last_name').val().trim();
        if(first_name == "" || first_name == null || last_name == "" || last_name == null) {
            alert("first name or last name is empty");
            return;
        }
        var travel_path = window.location.origin + '/transact/getReserByCustomerName';
        console.log(travel_path);
        $.ajax({
            url: travel_path,
            type: 'get',
            data: {'firstName': first_name, 'lastName': last_name},
            success: function(data) {
                try {
                    console.log(data);
                    $('#manager-table-id').empty();
                    $('#manager-table-id').html('Get List of Reservation by customer name');
                    $('#manager-table-head').empty();
                    $('#manager-table-body').empty();

                    print_data_to_table(data);
                    if (data.error || data == null) {
                        $('#manager-table-id').append('<p class=\"alert alert-warning"> Search does not exist</p>');
                    }
                }catch(err) {
                    $('#manager-table-id').append('<p class=\"alert alert-warning"> Search does not exist</p>');
                }
            }
        });
    });
});

function print_data_to_table (data) {
    if(data == null) {
        return;
    }
    var time_cons = -1;
    if(Array.isArray(data)) {
        var keys = Object.keys(data[0]);
        // $('#manager-table').append('<tr>');
        for(var i = 0; i < keys.length; i++) {
            $('#manager-table-head').append('<th>'+ keys[i] +'</th>');
            if(keys[i] == 'ResrDate') {
                time_cons = i;
            }
        }
        // $('#manager-table').append('</tr>');
        for(var i = 0; i < data.length; i++) {
            $('#manager-table-body').append('<tr>');
            var value = Object.values(data[i]);
            for(var j = 0; j < value.length; j++) {
                if(time_cons == j) {
                    value[j] = (new Date(value[j])).toLocaleString();
                }
                $('#manager-table-body').append('<td>'+ value[j] +'</td>');
            }
            $('#manager-table-body').append('</tr>');
        }
    } else {
        var keys = Object.keys(data);
        // $('#manager-table').append('<tr>');
        for(var i = 0; i < keys.length; i++) {
            $('#manager-table-head').append('<th>'+ keys[i] +'</th>');
        }

        $('#manager-table-body').append('<tr>');
        var value = Object.values(data);
        for(var j = 0; j < value.length; j++) {
            $('#manager-table-body').append('<td>'+ value[j] +'</td>');
        }
        $('#manager-table-body').append('</tr>');

    }
}